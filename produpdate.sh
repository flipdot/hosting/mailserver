#!/bin/bash

varstocheck=(
  PASSWORD_STORE_DIR
  HCLOUD_TOKEN
)

for var in "${varstocheck[@]}"; do
  if [[ -z ${!var+x} ]];
    then echo "$var is unset, this will leads to errors. Will exit now, see README.MD"
    exit 1
  fi
done

echo "Update Ansible roles and collections"
ansible-galaxy install -f -r requirements.yml

echo "Execute Ansible now"
ansible-playbook -u root -e "env=prod" -i inventory/hcloud.yml mailserver.yml
